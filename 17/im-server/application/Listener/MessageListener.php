<?php
/**
 * Created by PhpStorm.
 * User: Sixstar-Peter
 * Date: 2019/3/27
 * Time: 21:12
 */

namespace App\Listener;
use Firebase\JWT\JWT;
use Six\Core\WebSocket\WebSocketContext;

/**
 *
 * @Listener(ws.message)
 */
class MessageListener
{
    public  function  handle($event){
         $frame=$event['frame'];
         $redis=$event['redis'];
         $server=$event['server'];
         $data=json_decode($frame->data,true);
         switch ($data['method']){
             case 'server_broadcast' :
                 $request=WebSocketContext::get($frame->fd)['request'];
                 $token=$request->header['sec-websocket-protocol'];
                 $key='peter123456';
                 $arr=JWT::decode($token,$key,['HS256']);
                 $userInfo=$arr->data;
                 $key=$userInfo->service_url;
                 $service=$redis->SMEMBERS('im_service');
                 //给不在本机上的客户端,通过路由服务器广播消息(排除当前的机器)
                  foreach($service as $k=>$v ){
                        $url_data=json_decode($v,true);
                        if($url_data['ip'].':'.$url_data['port']==$key){
                          unset($service[$k]);
                        }
                  }
                 $cli = new \Swoole\Coroutine\http\Client("118.24.109.254", 9600);
                 $ret = $cli->upgrade("/"); //升级的websockt
                 if ($ret) {
                     $route_data=['method'=>'route_broadcast','target_server'=>$service,'msg'=>$data['msg']];
                     $cli->push(json_encode($route_data));
                 }
                 //如果说当前服务端连接了客户端那么就直接发
                  $this->sendAll($server,$data['msg']);
                 break;
             case 'route_broadcast' : //接收到服务器广播
                    $ack_data=['method'=>'server_ack','msg_id'=>$data['msg_id']];
                    $server->push($frame->fd,json_encode($ack_data)); //是给我发送消息的客户端
                    $server->close($frame->fd); //避免广播的时候得到额外
                    $this->sendAll($server,$data['msg']);
                 break;
         }
        var_dump("message事件触发");
    }

    public function  sendAll($server,$msg){
         foreach ($server->connections as $fd){
             if($server->exists($fd)){
                 $server->push($fd,$msg); //没有排除ack确认的客户端
             }
         }
    }

    public  function  send($data){
        $cli = new \Swoole\Coroutine\http\Client("118.24.109.254", 9600);
        $ret = $cli->upgrade("/"); //升级的websockt
        if ($ret) {
            $cli->push(json_encode($data));
        }
    }
}
