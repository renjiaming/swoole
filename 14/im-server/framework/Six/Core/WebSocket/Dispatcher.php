<?php
/**
 * Created by PhpStorm.
 * User: Sixstar-Peter
 * Date: 2019/3/21
 * Time: 22:51
 */

namespace Six\Core\WebSocket;

/**
 * 通过路径触发控制器当中的方法
 * Class Dispatcher
 * @package Six\Core\WebSocket
 */
class Dispatcher
{
    public static function  open($server,$request,$path){
         try{
             //通过路径得到类名然后实例化
              $className=self::getClassName($path);

              if($className!=null){
                  $obj=new $className;
                  $obj->open($server,$request);
              }


         }catch (\Throwable $t){
             var_dump($t->getMessage());
         }
    }

    public static function  message(){

    }

    public static  function  close(){

    }

    public static function getClassName($path=null){
             if($path==null){
                return null;
             }
             $nameSpace='App\WebSocket\\';
             $className=$nameSpace.ucfirst(strtolower(explode('/',$path)[1]));
             if(class_exists($className)){
                return $className;
             }
             return null;
    }
}