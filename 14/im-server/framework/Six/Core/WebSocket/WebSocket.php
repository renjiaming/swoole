<?php
/**
 * Created by PhpStorm.
 * User: Sixstar-Peter
 * Date: 2019/3/21
 * Time: 22:03
 */

namespace Six\Core\WebSocket;


use Six\Core\Config;
use Six\Core\Http;
use Swoole\WebSocket\Server;

class WebSocket extends Http
{
    public  function  run(){

        $config=Config::get_instance();
        $config->load(); //载入配置文件
        $setting=$config->get('ws');

        $this->server=new Server($setting['host'],$setting['port']);
        $this->server->set($setting['swoole_setting']);

        $this->server->on('Start',[$this,'start']);
        $this->server->on('workerStart',[$this,'workerStart']);

        $this->server->on('open',[$this,'open']);
        $this->server->on('message',[$this,'message']);
        $this->server->on('close',[$this,'close']);

        //启动http服务
        if ((int)$setting['enable_http'] === 1) {
            $this->server->on('request',[$this,'request']);
        }
        //tcp服务端口监听
        if ((int)$setting['tcpable'] === 1) {
            $tcp=$config->get('tcp');
            $this->registerTcp($tcp);
        }

        $this->server->start();

          //1.热重启

          //2.能够支持http请求,解析路由

          //3.能够拥有websocket控制器 ()
    }

    public  function open($server,$request){
            $path=$request->server['path_info'];
            $fd=$request->fd;
            WebSocketContext::init($fd,$path); //保存了上下文信息
            //触发到控制器,路由
            Dispatcher::open($server,$request,$path);

    }

    public  function message($server,$frame){
        //var_dump($request->server['path_info']);
        $fd=$frame->fd;
        $path=WebSocketContext::get($fd);
        var_dump($path);
    }

    public  function close($server,$fd){
       // var_dump($request->server['path_info']);
        $path=WebSocketContext::get($fd);
        var_dump($path);

    }


}