<?php


namespace Six\Core\Event;


class Event
{
    public static $events = [];

    /**
     * 事件注册
     * @param $event
     * @param $callback
     */
    public static function register($event, $callback)
    {
        $event = strtolower($event);
        if (!isset(self::$events[$event])) {
            self::$events[$event] = [];
        }

        self::$events[$event] = [
            'callback' => $callback
        ];
    }

    //事件的触发
    public static function trigger($event, $params = [])
    {
        $event = strtolower($event);
        if(call_user_func(self::$events[$event]['callback'], $params)) {
            return true;
        }
        return false;

    }

}