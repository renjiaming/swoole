<?php

namespace App\Listener;

/**
 * @Listener('start')
 */
class StartListener
{
    public function handle($params)
    {
        go(function () {
            $cli = new \Swoole\Coroutine\Http\Client('127.0.0.1', 9600);
            $ret = $cli->upgrade("/"); //升级websocket
            if ($ret) {
                $data = [
                    'method' => 'register',
                    'serviceName' => 'IM1',
                    'ip' => '127.0.0.1',
                    'port' => '9600'
                ];

                $cli->push(json_encode($data));

                //心跳处理
                swoole_timer_tick(3000, function () use ($cli) {
                    if ($cli->errCode == 0) {
                        $cli->push('', WEBSOCKET_OPCODE_PING);
                    }
                });
            }
        });
    }
}