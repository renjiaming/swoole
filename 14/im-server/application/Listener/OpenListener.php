<?php

namespace App\Listener;

/**
 * @Listener('open')
 */
class OpenListener
{
    public function handle()
    {
        var_dump("主进程启动事件触发");
    }
}