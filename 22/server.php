<?php
$serv = new Swoole\Http\Server("127.0.0.1", 9800, SWOOLE_BASE);
Swoole\Runtime::enableCoroutine(true); //一键协程化
include_once 'pool/Pool.php';

$serv->set(array(
    'worker_num' => 2,
    'task_worker_num' => 4,
));
$serv->on('request', function ($request, $response) {
    $pool= Pool::getInstance();
    $mysql = $pool->getConnection();
    $mysql->query('select sleep(3);');
    $response->end('协程结束');
    $pool->freeConnection($mysql);

});

$serv->on('workerStart', function ($serv, $worker_id) {
    Pool::getInstance()->init();
    Pool::getInstance()->gcConnection();
});

$serv->on('Receive', function (Swoole\Server $serv, $fd, $from_id, $data) {

});

$serv->on('Task', function (Swoole\Server $serv, $task_id, $from_id, $data) {

});

$serv->on('Finish', function (Swoole\Server $serv, $task_id, $data) {

});


$serv->start();